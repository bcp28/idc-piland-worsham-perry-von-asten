
#include <Servo.h>  // Include Servo Library

#define Rx 17 // DOUT to pin 17
#define Tx 16 // DIN  to pin 16

// Declare LED pins
#define RED_PIN 7
#define GREEN_PIN 8
#define BUTTON_PIN 9

// Declare QTI pins
#define RIGHT_QTI_PIN 52
#define CENTER_QTI_PIN 53
#define LEFT_QTI_PIN 51

// Declare white/black min/max
#define WHITE_MAX 40

#define LOOP_SPEED 50 // Hz; number of times loop runs per second

int loopDelay = 1000 / LOOP_SPEED; // Calculate the loop delay from the loop speed

enum STATES { NONE, CHARACTER_SENT, CHARACTER_RECEIVED, PAUSING_DRIVE }; // Define an emueration of possible states
int NEXT_STATE_AFTER[] {NONE, NONE, NONE, NONE }; // What to do after each state terminates
int STATE_TIMEOUTS[] { -1, 1000, 1000, 1000 }; // How long we should wait before exiting state

enum QTI_PINS { LEFT, CENTER, RIGHT }; 
enum DIRECTIONS { DIR_LEFT, DIR_STRAIGHT, DIR_RIGHT, DIR_PAUSE, DIR_STOP };

Servo servoLeft;    // Declare Left and Right Servos
Servo servoRight;

void setup() {

    Serial.begin(9600);  // Communicate with the host computer at 9600 baud
    Serial2.begin(9600); // Communicate with the XBee antenna at 9600 baud
    
    servoLeft.attach(12);
    servoRight.attach(11);

    // Initialize pins to output/input
    pinMode(GREEN_PIN, OUTPUT);
    pinMode(RED_PIN, OUTPUT);
    pinMode(BUTTON_PIN, INPUT);

    pinMode(RIGHT_QTI_PIN, INPUT);
    pinMode(CENTER_QTI_PIN, INPUT);
    pinMode(LEFT_QTI_PIN, INPUT);

    delay(500); // Wait 500 ms before starting to check for signals (this might be removed later)
}

int stateClockMillis = 0; // Keep track of how long we have been in a given state
long lastMillis = 0;
int currentState = NONE;  // The initial state is "not doing anything"
long sensorReadings[] = { 0, 0, 0 }; 

void loop() {
    
    if (currentState == NONE && Serial2.available()) { // If we received a character, and weren't in the process of doing something else
        while (Serial2.available()) Serial2.read(); // Read the character (clear it from the buffer)
        Serial.println("Received message"); // Print to Serial monitor
        setState(CHARACTER_RECEIVED); // Change state to "character received" - prevents another character from being sent for 1 second
        digitalWrite(GREEN_PIN, HIGH); // Turn on the green light (for 1 second)
    }

    if (currentState != NONE) { // If we are actively doing something
        if (stateClockMillis >= STATE_TIMEOUTS[currentState]) { // Check if time has expired
            setState(NEXT_STATE_AFTER[currentState]); // Change state to whatever would be next
        } else {
            long currentTime = millis(); // Retrieve the current time
            stateClockMillis += (currentTime - lastMillis); // Add the elapsed time since the last loop
            lastMillis = currentTime; // Set the last time to the current time
        }
    }

    if (digitalRead(BUTTON_PIN) && currentState == NONE) { // If someone pressed the button, and we weren't in the process of doing something else
        Serial.println("Sending message..."); // Print to Serial monitor
        setState(CHARACTER_SENT); // Change state to "character sent" - prevents another character from being sent for 1 second
        digitalWrite(RED_PIN, HIGH); // Turn on the red light (for 1 second)
        Serial2.print('~'); // Send a character
    }

    int intendedDirection = getIntendedDirection();

    switch (intendedDirection) {
        case DIR_LEFT:
            turnLeft();
            break;
        case DIR_RIGHT:
            turnRight();
            break;
        case DIR_STRAIGHT: 
            if (currentState != PAUSING_DRIVE) goStraight();
            break;
        case DIR_PAUSE:
            setState(PAUSING_DRIVE);
            stopMotors();
            break;
        case DIR_STOP:
        default:
            stopMotors();
    }

    delay(loopDelay); // Wait for the previously calculated amount of time
}

void clearLeds() {
    // Set the red and green LED output pins to LOW
    digitalWrite(GREEN_PIN, LOW);
    digitalWrite(RED_PIN, LOW);
}

long RCtime(int sensPin){

   long result = 0;

   pinMode(sensPin, OUTPUT);       // make pin OUTPUT

   digitalWrite(sensPin, HIGH);    // make pin HIGH to discharge capacitor - study the schematic

   delay(1);                       // wait a  ms to make sure cap is discharged

   pinMode(sensPin, INPUT);        // turn pin into an input and time till pin goes low

   digitalWrite(sensPin, LOW);     // turn pullups off - or it won't work

   while(digitalRead(sensPin)){    // wait for pin to go low

      result++;

   }

   return result;                   // report results
}

int getIntendedDirection() {
    getSensorReadings();

    bool leftIsBlack = (sensorReadings[LEFT] > WHITE_MAX);
    bool centerIsBlack = (sensorReadings[CENTER] > WHITE_MAX);
    bool rightIsBlack = (sensorReadings[RIGHT] > WHITE_MAX);
    
    // Serial.print(sensorReadings[LEFT]);
    // Serial.print(","); 
    // Serial.print(sensorReadings[CENTER]);
    // Serial.print(","); 
    // Serial.println(sensorReadings[RIGHT]);

    Serial.print(leftIsBlack);
    Serial.print(centerIsBlack);
    Serial.println(rightIsBlack);

    if (!centerIsBlack) return DIR_STOP;

    if (leftIsBlack && rightIsBlack) return DIR_PAUSE;

    if (rightIsBlack) return DIR_RIGHT;
    if (leftIsBlack) return DIR_LEFT;
    
    return DIR_STRAIGHT;
}

void turnRight() {
    servoLeft.writeMicroseconds(1700);         // Left wheel counterclockwise
    servoRight.writeMicroseconds(1700);        // Right wheel counterclockwise
}

void turnLeft() {
    servoLeft.writeMicroseconds(1300);         // Left wheel clockwise
    servoRight.writeMicroseconds(1300);        // Right wheel clockwise
}

void goStraight() {
    servoLeft.writeMicroseconds(1700);         // Left wheel counterclockwise
    servoRight.writeMicroseconds(1300);        // Right wheel clockwise
}

void stopMotors() {
    servoLeft.writeMicroseconds(1500);         // Left wheel stop
    servoRight.writeMicroseconds(1500);        // Right wheel stop
}

void getSensorReadings() {
    sensorReadings[LEFT] = RCtime(LEFT_QTI_PIN);
    sensorReadings[CENTER] = RCtime(CENTER_QTI_PIN);
    sensorReadings[RIGHT] = RCtime(RIGHT_QTI_PIN);
}

void setState(int state) {
    currentState = state; // Set the current state to `state`
    stateClockMillis = 0; // Reset the state clock
    clearLeds(); // Turn off all the LEDs
}